#include <fstream>
#include <map>
#include <iostream>
using namespace std;

int main(int argc, char* argv[]){

std::ifstream infile(argv[1]);
    if(infile.is_open()) {
		map<int, int> countMap;
		int part = 0;
		int sum = 0;
		int count = 0;
        while(!infile.eof()){
			infile >> part;
			if(countMap.find(part) != countMap.end()){
				countMap[part] = countMap[part] + 1;
			}
			else{
				countMap[part] = 1;
			}
			count++;
        }
        infile.close();

		for(map<int, int>::iterator itr = countMap.begin(); itr != countMap.end(); itr++){
			cout << itr->first << "," << itr->second << endl;
			sum = sum + itr->second;
		}
		cout << "total vertices " << sum <<endl;
		cout << "total count " << count <<endl;
        return 0;
    } 

}
