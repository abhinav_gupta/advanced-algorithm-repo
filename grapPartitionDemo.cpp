#include "GraphReader.h"
#include "Graph.h"
#include <stdlib.h>
#include "Partition.h"
#include <iostream>
#include <ctime>
#include <sstream>
#include <vector>

using namespace std;
#define GEN_UNDIR 1
#define GEN_METIS 0

void usage(){
	cout << "usage: grapPartitionDemo <-h|-p|-g> <graph-input-file>  <graph-output-file|heurtic type> <sort-option|K>" << endl;
	cout << "-p = to partition, -g = to generate the graph, -h = for help" <<endl;
	cout << "ex: grapPartitionDemo -p sample Balanced|Hashing|DeterministicGreedy:LinearWeighted 5" << endl;
	cout << "ex: grapPartitionDemo -g sample out 0|1" << endl;
}
vector<string> &split(const string &s, char delim, vector<std::string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

vector<string> split(const string &s, char delim) {
    vector<std::string> elems;
    return split(s, delim, elems);
}

htype getHtype(string args){
	vector<string> tokens;
    split(args, ':', tokens);
	string htype = tokens[0];
	cout << htype << endl;
	if(htype.compare("Hashing") == 0){
		return Hashing;
	}
	if(htype.compare("Balanced") == 0){
		return Balanced;
	}
	if(htype.compare("DeterministicGreedy") == 0){
		return DeterministicGreedy;
	}
	else{
		cout << "Unknown type" << endl;
		usage();
		exit(0);
	}

}

WeightType getWeightType(string args){
	vector<string> tokens;  
    split(args, ':', tokens);
	if(tokens.size() > 1) {
			string weightType = tokens[1];
			if(weightType.compare("LinearWeighted") == 0){
					return LinearWeighted;
			}
			else if(weightType.compare("ExpWeighted") == 0){
					return ExpWeighted;
			}
	}
	return UnweightedGreedy;
}

int main(int argc, char *argv[]){
	if(argc < 5){
		usage();
		//cout << "usage: grapPartitionDemo <-p|-g> <graph-input-file>  <graph-output-file|heurtic type> <sort-option|K>" << endl;
		return 1;
	}

	try {
		//int K = atoi(argv[4]), C = atoi(argv[5]);
		
		int K = 12, C = 500;

		string argtype = argv[1];
		GraphReader *gr = new GraphReader();
		string infile(argv[2]);
		Graph *g = NULL;

		if(argtype.compare("-g") == 0){
				string outfile(argv[3]);
				if(GEN_UNDIR) {
						g = gr->readDir(infile, outfile);
						g->dumpUnsort(outfile);

						if(atoi(argv[4]) == 1) {
								g = gr->readUndir(infile, outfile);
								g->dumpSort(outfile);
						}
						if(GEN_METIS) {
								g = gr->readUndir(infile, outfile);
								g->writeAsMetis(outfile);
						}
				}
		}
		else if(argtype.compare("-p") == 0){
				string htype(argv[3]);
				K = atoi(argv[4]);
				g = gr->read(infile);
				//g->partition(K, C, Hashing);
				//g->partition(K, C, Balanced);
				//g->setDelayAssign(true);
				g->partition(K, C, getHtype(htype), getWeightType(htype));
				//g->partition(K, C, DeterministicGreedy, UnweightedGreedy);
				//g->partition(K, C, DeterministicGreedy, LinearWeighted);
				g->printAllPartition();
		}
		else{
			usage();
			return 1;
		}
	
		delete g;
		delete gr;	
	}
	catch (...){
		cout << "invalid file name or graph input format" << endl;
	}
	return 0;
}



