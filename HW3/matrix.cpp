#include "matrix.h"

#if 0
void matrix::Iter_MM_ijk() {
	int i,j,k;

	for(i=0; i<matrix_sz; i++) 
		for(j=0; j<matrix_sz; j++) 
			for(k=0; k<matrix_sz; k++) 

				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
}


void matrix::Iter_MM_ikj() {
	int i,j,k;
    
	for(i=0; i<matrix_sz; i++) 
		for(k=0; k<matrix_sz; k++) 
			for(j=0; j<matrix_sz; j++)
 
				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
	
}


void matrix::Iter_MM_jik() {
	int i,j,k;
    
	for(j=0; j<matrix_sz; j++) 
		for(i=0; i<matrix_sz; i++) 
			for(k=0; k<matrix_sz; k++)
 
				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
	
}


void matrix::Iter_MM_jki() {
	int i,j,k;
    
	for(j=0; j<matrix_sz; j++) 
		for(k=0; k<matrix_sz; k++) 
			for(i=0; i<matrix_sz; i++)
 
				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
	
}

void matrix::Iter_MM_kij() {
	int i,j,k;
    
	for(k=0; k<matrix_sz; k++) 
		for(i=0; i<matrix_sz; i++) 
			for(j=0; j<matrix_sz; j++)
 
				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
	
}

void matrix::Iter_MM_kji() {
	int i,j,k;
    
	for(k=0; k<matrix_sz; k++) 
		for(j=0; j<matrix_sz; j++) 
			for(i=0; i<matrix_sz; i++)
 
				*(mz + (i*matrix_sz) + (j)) = *(mz + (i*matrix_sz) + (j)) + 
					*(mx + (i*matrix_sz) + (k)) * *(my + (k*matrix_sz) + (j));
	
}
#endif

void matrix::read_matrix(string fname, int m_type) {
    ifstream file;
    file.open(fname.c_str());
    if(file.is_open()) {
		int  num;
		file >> matrix_sz;

		cout << "Matrix sz :" << matrix_sz << endl;
		if(m_type == 1)
	        mx = (int *)malloc(sizeof(int) * matrix_sz * matrix_sz);
		else if (m_type == 2) 
	        my = (int *)malloc(sizeof(int) * matrix_sz * matrix_sz);
		
        for (int i=0; i<(matrix_sz * matrix_sz); i++) {
            file >> num;
			if(m_type==1)
				mx[i] = num;
			else if(m_type == 2)
				my[i] = num;
        }
        file.close();
    } else {
        cout << "Error in opening file" << endl;
    }
    return;
}


void matrix::dump_matrix(int *matrix) {
	for(int i=0; i < (matrix_sz * matrix_sz); i++) {
		cout << matrix[i] << "\t";
	}
	cout << endl;
}


void matrix::mz_alloc() {
	mz = (int *)malloc(sizeof(int) * matrix_sz * matrix_sz);
}
void matrix::mz_init() {
	for (int i=0; i<(matrix_sz * matrix_sz); i++) 
		mz[i] = 0;
}


int main(int argc, char* argv[] ) {
    if(argc >= 3) {
		matrix M;

		M.read_matrix(argv[1], 1);
		M.read_matrix(argv[2], 2);

		cout<< " Matrix 1:" <<endl;
		M.dump_matrix(M.mx);
		cout<< " Matrix 2:" <<endl;
		M.dump_matrix(M.my);

		M.mz_alloc();
		M.mz_init();


		cout << "Starting" <<endl;
		matrixinfo x(0, 0 , M.matrix_sz);
		matrixinfo y(0, 0 , M.matrix_sz);
		matrixinfo z(0, 0 , M.matrix_sz);

		 M.recMM(x, y, z);
		cout<< " Matrix 3:" <<endl;
		M.dump_matrix(M.mz);
		
#if 0

		auto start = chrono::high_resolution_clock::now();
		M.Iter_MM_ijk();
		auto finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);
		
		M.M3_init();
		start = chrono::high_resolution_clock::now();
		M.Iter_MM_ikj();
		finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);
		

		M.M3_init();
		start = chrono::high_resolution_clock::now();
		M.Iter_MM_jik();
		finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);
		

		M.M3_init();
		start = chrono::high_resolution_clock::now();
		M.Iter_MM_jki();
		finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);
		

		M.M3_init();
		start = chrono::high_resolution_clock::now();
		M.Iter_MM_kij();
		finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);
		

		M.M3_init();
		start = chrono::high_resolution_clock::now();
		M.Iter_MM_kji();
		finish = chrono::high_resolution_clock::now();
		cout<<chrono::duration_cast<chrono::nanoseconds>(finish-start).count()<<"ns\n";
		M.dump_matrix(M.mz);

#endif

    } else {
        cout<<"Usage: ./matrix <input1-filename> <input2-filename>" << endl;
    }

    return 0;
}


