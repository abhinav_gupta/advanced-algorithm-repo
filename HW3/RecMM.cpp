#include "matrix.h"

int bsz = 2;

void matrix::recMM(matrixinfo x, matrixinfo y, matrixinfo z){
	int nsz = x.n;

	if(nsz <= bsz){
		//call_bestalgo;
		Iter_MM_ijk(x, y, z);
		return;
	}
	nsz = x.n/2;

	matrixinfo x11(x.i, x.j, nsz), y11(y.i, y.j, nsz), z11(z.i, z.j, nsz);
	matrixinfo x12(x.i , x.j + nsz, nsz), y12(y.i, y.j + nsz, nsz), z12(z.i, z.j + nsz, nsz);
	matrixinfo x21(x.i + nsz, x.j, nsz), y21(y.i + nsz, y.j, nsz), z21(z.i + nsz, z.j, nsz);
	matrixinfo x22(x.i + nsz, x.j + nsz, nsz), y22(y.i + nsz, y.j + nsz, nsz), z22(z.i + nsz, z.j + nsz, nsz);

	recMM(x11, y11, z11);
	recMM(x12, y21, z11);
	recMM(x11, y12, z12);
	recMM(x12, y22, z12);
	recMM(x21, y11, z21);
	recMM(x22, y21, z21);
	recMM(x21, y12, z22);
	recMM(x22, y22, z22);
}

void matrix::Iter_MM_ijk(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
	
    for(i= 0; i<sz; i++) {
        for(j = 0 ; j<sz; j++) {
            for(k = 0; k<sz; k++) {
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
			}
		}
	}
}



void matrix::Iter_MM_ikj(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
    
	for(i= 0; i<sz; i++) 
        for(k = 0 ; k<sz; k++) 
            for(j = 0; j<sz; j++) 
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
}


void matrix::Iter_MM_jik(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
	
    for(j= 0; j<sz; j++) 
        for(i = 0 ; i<sz; i++) 
            for(k = 0; k<sz; k++) 
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
}



void matrix::Iter_MM_jki(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
    
	for(j= 0; j<sz; j++) 
        for(k = 0 ; k<sz; k++) 
            for(i = 0; i<sz; i++) 
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
}


void matrix::Iter_MM_kij(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
	
    for(k= 0; k<sz; k++) 
        for(i = 0 ; i<sz; i++) 
            for(j = 0; j<sz; j++) 
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
}



void matrix::Iter_MM_kji(matrixinfo x, matrixinfo y, matrixinfo z) {
	int sz = z.n;
	int i, j, k;
    
	for(k= 0; k<sz; k++) 
        for(j = 0 ; j<sz; j++) 
            for(i = 0; i<sz; i++) 
				mz[(z.i + i)*matrix_sz + (z.j + j)] += mx[(x.i + i)*matrix_sz + 
								(x.j + k)]*	my[(y.i + k)*matrix_sz + (y.j + j)];
}

