#include<iostream>
#include<string.h>
#include<stdlib.h>
#include "GraphReader.h"
#include "Graph.h"
using namespace std;
#define MAX_FILE_LEN 50

class ConvertGraph {
public:
	void init(int is_sort);
	int getFileInfo(char* infile, char* outfile);
	void print_info();
	void dump_graph(Graph *G);
private:
	char infileName[MAX_FILE_LEN];
	int infileLen;
	char outfileName[MAX_FILE_LEN];
	int outfileLen;
	int is_sorted;
};

void ConvertGraph::init(int is_sort) {
	infileLen = 0;
	outfileLen = 0;
	is_sorted = is_sort;
}

int ConvertGraph::getFileInfo(char *infile, char *outfile) {
	infileLen = strlen(infile);
	strcpy(infileName, infile);
	infile[infileLen] = '\0';

	outfileLen = strlen(outfile);
	strcpy(outfileName, outfile);
	outfile[outfileLen] = '\0';
	return 1;
}

void ConvertGraph::dump_graph(Graph *G) {
/*
	Graph *graph = G;
	cout<<"Num vertices :"<<nvertices<<" Num edges:"<<nedges<<endl; 
	
	for(int i=0; i<nvertices; i++) {
		vector<int> 	
	}
*/
	return;
}


void ConvertGraph::print_info() {
	cout<<"Infile :"<<infileName<<endl;
	cout<<"Outfile :"<<outfileName<<endl;
}

//Input: exe <input-file> <output-file> <is_sorted>
int main(int argc, char *argv[]) {
	if(argc<=3) {
		cout<<"Usage: exe <input-file> <output-file> <is_sorted(1 or 0)>"<<endl;
		exit(1);
	} else {
		ConvertGraph CG;
		CG.init(atoi(argv[3]));
		CG.getFileInfo(argv[1], argv[2]);
		GraphReader *gr = new GraphReader();
		Graph *Graph = gr->read(argv[1]);
		CG.dump_graph(Graph);
		CG.print_info();
	}	
	
	return 0;
}
