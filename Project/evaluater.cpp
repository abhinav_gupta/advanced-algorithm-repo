#include "Evaluate.h"

void Evaluater::cntEdgecuts(string &infilename, int partitions) {
	std::ifstream infile(infilename.c_str());
	string line;
	K = partitions;
	int vertex = 0;
	if(infile.is_open()) {
		partitionCnt.resize(K);
		while ( infile.good()) {
			getline (infile,line);
			partitionCnt[atoi(line.c_str())].push_back(vertex); 
			//cout << line << endl;
			vertex++;
		}
		for(int i=0; i<partitionCnt[0].size(); i++) {
			cout<<partitionCnt[0][i]<<"\t";
		}
		cout<<"Partition 0:"<<partitionCnt[0].size()<<endl;
		cout<<"Partition 1:"<<partitionCnt[1].size()<<endl;
		infile.close();
	} else {
		cout<<"Error: Unable to open file."<<endl;
		exit(1);
	}
	
}

#if 0
int main(int argc, char *argv[]) {
	if(argc > 2) {
		string infile(argv[1]);
		Evaluater eval;
		int partitions = atoi(argv[2]);
		eval.cntEdgecuts(infile, partitions);
	} else {
		cout<<"Usage: evaluate <input-file> K"<<endl;
		exit(1);
	}
	return 0;
}
#endif
