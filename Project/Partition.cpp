#include "Partition.h"
#include <iostream>
#include<math.h>
using namespace std;


Partition::Partition(){
	curSize = 0;
}
Partition::~Partition(){}
int 
Partition::size(){
	return vtoadjList.size(); 
	//return curSize;
}

void Partition::addVertex(int v, vector<int> &neighbors ) {
        vtoadjList[v] = neighbors;         
		curSize++;
}

void Partition::printPartition() {
	for( map<int, vector <int> >::iterator ii=vtoadjList.begin(); ii!=vtoadjList.end(); ++ii) {
		//cout <<"Vertex :"<< (*ii).first <<"->";
		vector<int> neighbors = (*ii).second;
		for(int i=0; i<neighbors.size();i++) {
			cout << neighbors[i] << ",";
		}
		cout<<endl;
	}
}

map<int, vector <int> >& Partition::getVtoAdjList(){	
	return vtoadjList;
}


int 
Partition::getCrossEdges() {
	int ncrossedges = 0;

	//cout << "********** PARTITION START *************" << endl;
	for( map<int, vector <int> >::iterator ii=vtoadjList.begin(); ii!=vtoadjList.end(); ++ii) {
                vector<int> neighbors = (*ii).second;
                for(int i=0; i<neighbors.size();i++) {
                	int u = (*ii).first;
			int v = neighbors[i];
			if(vtoadjList.end() == vtoadjList.find(v)){
				//cout << " cross edge: {" << u << ", " << v << "}" << endl;
				ncrossedges++;
			}
		}
        }

	//cout << "********** PARTITION END **************" << endl;
	return ncrossedges;
}
