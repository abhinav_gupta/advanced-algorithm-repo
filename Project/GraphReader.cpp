#include "GraphReader.h"
#include<stdlib.h>
#include <fstream>
#include <iostream>

using namespace std;



Graph* GraphReader::readDir(string &infile, string &outfile) {
	read(infile);
}

Graph* GraphReader::readUndir(string &infile, string &outfile) {
	read(outfile);
}


Graph* GraphReader::read(string &filename){

	std::ifstream infile(filename.c_str());
	if(infile.is_open()) {
		int vertices = 0, edges = 0, sources = 0;

		infile >> vertices >> edges >> sources;
		Graph *g = new Graph(vertices, edges, sources);

		int iedge = 0;
		while(iedge < edges){
			int u = 0, v = 0;
			infile >> u >> v;
			g->addEdge(u, v);
			iedge++;		
		}


		int isource = 0;
		while(isource < sources){
			int s = 0;
			infile >> s;
			g->addSource(s);
			isource++;
		}
		infile.close();
		return g;
	} else {
		cout<<"Issue in opening file."<<endl;
		exit(1);
	}

	return NULL;

}
