#ifndef _GRAPH_H
#define _GRAPH_H
#include <vector>
#include"Partition.h"
#include <string>
#include <stack>

using namespace std;

enum htype {
                Hashing         = 0,
                Balanced      = 1,
		DeterministicGreedy = 2
        };

enum WeightType {
		UnweightedGreedy = 0,
		LinearWeighted = 1,
		ExpWeighted = 2
	};


class Graph {
public:
	Graph(int numvertices, int numedges, int numsources);
	Graph();
	~Graph();
	void addEdge(int u, int v);
	void addSource(int s);
	vector<int>& getNeighbors(int u);
	vector<int>& getSources();
	vector<int> order;
	int numedges();
	int numvertices();
	int numsources();

	bool partition(int K, int C, htype type, WeightType wt = UnweightedGreedy);
	void printAllPartition();
	int numCrossEdges();
	void writeAsMetis(string &filename);

	float getUnweightedGreedyWeight(int pid, int C);
	float getExpoGreedyWeight(int pid, int C);
	bool deterministicGreedy(int K, WeightType wt);
	float getLinearGreedyWeight(int pid, int C);

	void dumpUnsort(string &outfile);
	void dumpSort(string &outfile);
	void dumpGraph(string &outfile, int is_sorted);
	void dumpVertexAndPartition();
	void get_bfs_order(string outfilename);
	void get_dfs_order(string outfilename);


private:
	bool hashPartition(int k);
	bool balancedPartition(int k);
	vector<Partition> partitions;
	vector<int> sources;
	vector<vector<int> > edges;
	int nvertices;
	int nedges;
	int nsources;
};

#endif // _GRAPH_H

