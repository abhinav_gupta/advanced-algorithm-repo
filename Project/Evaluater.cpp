#include "Evaluater.h"
#include <assert.h>
#include <algorithm>
#include "Graph.h"
#include <unordered_map>
#include <string>


void Evaluater::cntEdgecuts(string &infilename, int partitions, Graph *gr) {
	std::ifstream infile(infilename.c_str());
	if(infile.is_open()) {
		string line;
		K = partitions;
		int vertex = 0;
		int edgecuts = 0;

		unordered_map<int, int> vPart;
		while (vertex < gr->numvertices()) {
			vertex++;
			getline (infile,line);
			vPart[vertex] = atoi(line.c_str());
		}

		for(int i=1; i<=vertex; i++) {
			vector<int> &neighbors = gr->getNeighbors(i);
			for(int l=0; l<neighbors.size(); l++) {
				if(vPart[i] != vPart[neighbors[l]]) {
					edgecuts++;
				}

			}
		}

		cout<<"\n\nEdge count :"<<edgecuts/2<<endl;
		infile.close();
	} else {
		cout<<"Error: Unable to open file."<<endl;
		exit(1);
	}
	
}

