#ifndef _EVALUATE_H
#define _EVALUATE_H
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <fstream>
#include "Graph.h"
using namespace std;

class Evaluater {
public:
        void cntEdgecuts(string &infilename, int K, Graph *g);
private:
        int K;
        vector<vector <int> >partitionCnt;
};


#endif
