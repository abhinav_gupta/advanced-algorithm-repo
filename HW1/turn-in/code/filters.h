#ifndef _FILTER_H
#define _FILTER_H

#include "Graph.h"
#include <fstream>
#include <list>
#include <cilk_mutex.h>
#include "util.h"

class filter {
public:
	filter(){}
	~filter(){}

protected:
	template <class T>
	bool all_queues_empty(QueueEx<T> *queues, int qlen);

	unsigned long long
	parComputeChecksum(vector<int> *d);	

	unsigned long long 
	computeChecksum(vector<int> &d);
};	




class bfs_serial : public filter{
public:
	bfs_serial();
	~bfs_serial();

	void execute(Graph *g, string outfile);
private:
	void _bfs_source(Graph *g, int s, fstream &outfile);
	
};

class bfs_parallel : public filter{
public:
	bfs_parallel (int cores, int maxstealattempts, int minstealsize);
	~bfs_parallel ();

	void execute(Graph *g, string outfile);
	void setparam(int cores, int maxstealattempts, int minstealsize);
private:
	void _bfs_source(Graph *G, int s, int &src_bfs_level, unsigned long long &checksum);
	void _bfs_thread(int thread_num, Graph* G, QueueEx<int> *qin, QueueEx<int> *qo,
		 		vector<int> *d, vector<cilk::mutex> &m);//, int &src_bfs_level);
private:
	int _ncores;
	int _maxstealattempts;
	int _minstealsize;


};
#endif // _FILTER_H
