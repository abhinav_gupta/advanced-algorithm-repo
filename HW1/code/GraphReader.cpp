#include "GraphReader.h"
#include <fstream>
#include <iostream>

/*GraphReader* GraphReader::gr = NULL;


GraphReader* GraphReader::getGraphReader()
{
	if(gr == NULL){
		gr = new GraphReader();
	}
	return gr;
}*/

using namespace std;
Graph* GraphReader::read(string filename){
	std::ifstream infile(filename.c_str());
	int vertices = 0, edges = 0, sources = 0;

	infile >> vertices >> edges >> sources;
	Graph *g = new Graph(vertices, edges, sources);

	int iedge = 0;
	while(iedge < edges){
		int u = 0, v = 0;
		infile >> u >> v;
		g->addEdge(u, v);
		iedge++;		
	}


	int isource = 0;
	while(isource < sources){
		int s = 0;
		infile >> s;
		g->addSource(s);
		isource++;
	}

	return g;
}
