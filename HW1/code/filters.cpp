#include "filters.h"
#include <queue>
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <exception>
#include <reducer_opadd.h>
#include <reducer_max.h>
#include <cilkview.h>
#include <reducer_opand.h>
using namespace std;


template<class T>
bool
filter::all_queues_empty(QueueEx<T> *queues, int qlen){
#if 0
        cilk::reducer_opand<bool> result;
	result.set_value(true);
	cilk_for (int i = 0; i < qlen; i++){
                result &= queues[i].empty();
        }

        return result.get_value();
#else
	for(int i = 0; i < qlen; i++){
		if(!queues[i].empty()){
			return false;
		}
	}
	return true;
#endif
}


unsigned long long
filter::parComputeChecksum(vector<int> *d){
        cilk::reducer_opadd<unsigned long long  > chksum;
	int size = d->size();
        cilk_for (int i = 1; i < size; i++){
                chksum += (*d)[i];
        }

        return chksum.get_value();
}


unsigned long long 
filter::computeChecksum(vector<int> *d){
	unsigned long long chksum = 0;
	int size = d->size();
	for(int i = 1; i < size; i++){
		chksum += (*d)[i];
	}

	return chksum;
}	


bfs_serial::bfs_serial(){}
bfs_serial::~bfs_serial(){}

void
bfs_serial::execute(Graph *g, string outfilename){
	if(g == NULL){
		std::cout << "fatal error: g is null" << endl;
		return;
	}
	std::vector<int> &sources = g->getSources();
	fstream outfile(outfilename.c_str(), ios_base::out);
	for(int i = 0; i < sources.size(); i++){
		_bfs_source(g, sources[i], outfile);
	}

}

void
bfs_serial::_bfs_source(Graph *g, int s, fstream &outfile){
	int nvertices = g->numvertices();
	vector<int> *d = new vector<int>(nvertices + 1);
	int src_bfs_level = 0;

	for (int i = 0; i < nvertices + 1; i++){
		(*d)[i] = nvertices;
	}

	(*d)[s] = 0;
	queue<int> q;
	q.push(s);


	while (!q.empty()){
		int u = q.front();
		q.pop();

		std::vector<int> &neighbors = g->getNeighbors(u);
		for(int i = 0; i < neighbors.size(); i++){
			if((*d)[neighbors[i]] == nvertices) {
				(*d)[neighbors[i]] = (*d)[u] + 1;
				src_bfs_level = (*d)[u] + 1;
				q.push(neighbors[i]);
			}
		}
	}

	unsigned long long checksum = computeChecksum(d);
	//cout << "checksum is " << checksum << " and src_bfs_level is " << src_bfs_level << endl;
	outfile << src_bfs_level << " " << checksum << endl;
}
	


bfs_parallel::bfs_parallel(int cores, int maxstealattempts, int minstealsize)
: _ncores(cores), _maxstealattempts(maxstealattempts), _minstealsize(minstealsize), _hdegree(cores*cores) {}

bfs_parallel::~bfs_parallel(){}

void
bfs_parallel::execute(Graph *g, string outfilename){
        if(g == NULL){
                std::cout << "fatal error: g is null" << endl;
                return;
        }
	cout << "inputs are " << _ncores << " " << _maxstealattempts << " " << _minstealsize << endl;
	_g = g;
        std::vector<int> &sources = g->getSources();
	int nsources = sources.size();
	vector<int> vsrclevel(nsources);
	vector<unsigned long long> vchecksum(nsources);

	int start_time = cilk::get_milliseconds();
        cilk_for (int i = 0; i < nsources; i++){
                _bfs_source(g, sources[i], vsrclevel[i], vchecksum[i]);
        }
	cout << "bfs_parallel execution time is " << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;
	fstream outfile(outfilename.c_str(), ios_base::out);
	for(int i = 0 ; i < nsources; i++){
		outfile << vsrclevel[i] << " " << vchecksum[i] << endl;
	}

}

void
bfs_parallel::_bfs_thread(int thread_num, Graph* G, QueueEx<int> *qin, QueueEx<int> *qo, vector<int> *d, vector<cilk::mutex> &m, queue<int> *hd)
{
        int nvertices = G->numvertices();

        while(1){
                while(!qin[thread_num].empty()){
                        int u = qin[thread_num].pop_front();
			std::vector<int> &neighbors = G->getNeighbors(u);
			int sz_n = neighbors.size();
			if(sz_n > _hdegree){
				hd->push(u);
				continue;
			}
			for(int i = 0; i < sz_n; i++){
				if((*d)[neighbors[i]] == nvertices){
					//src_bfs_level = (*d)[u] + 1;
					(*d)[neighbors[i]] = (*d)[u] + 1;//src_bfs_level;
					qo->push_back(neighbors[i]);
				}
			}

                }
                int t = 0;//_maxstealattempts;
                m[thread_num].lock();
                srand(time(NULL));
                while(qin[thread_num].empty() && t < _maxstealattempts){
                        int r = rand() % (_ncores - 1);
			r = r < thread_num ? r : r + 1;
			//int r = rand() % (_ncores);
			if(/*r != thread_num && */m[r].try_lock()){
				int rqsize = qin[r].size();
				if(rqsize > _minstealsize) {
					// removing steal function bcoz copying issue
					qin[thread_num].steal(qin[r]);
				}
				m[r].unlock();
                        }
                        t++;
                }
                m[thread_num].unlock();

		if (qin[thread_num].empty()){
			break;
		}
        }
}

void 
bfs_parallel::_bfs_source(Graph *G, int s, int &src_bfs_level, unsigned long long &checksum){
        int nvertices = G->numvertices();
	vector<int> *d = new vector<int>(nvertices + 1);
        cilk_for (int i = 0; i < nvertices + 1; i++){
                (*d)[i] = nvertices;
        }

        (*d)[s] = 0;
	QueueEx<int> *qin = new QueueEx<int>[_ncores];
	queue<int> *hq = new queue<int>[_ncores];
	vector<cilk::mutex>  m(_ncores);
	
        qin[0].push_back(s);
	src_bfs_level = -1;
        while (!all_queues_empty(qin , _ncores)){
                QueueEx<int> *qout = new QueueEx<int>[_ncores];
		for(int i=0; i < (_ncores-1); i++){
                        cilk_spawn _bfs_thread(i, G, qin, qout + i, d, m, hq + i);//, src_bfs_level);
                }
                _bfs_thread(_ncores-1, G, qin, qout + _ncores-1, d, m, hq + _ncores - 1);//,  src_bfs_level);
                cilk_sync;
		// proces shigh deg vertices
		int count = 0;
		for(int i = 0; i < _ncores; i++){
			
			while(!hq[i].empty()){
				count++;
				int u = (hq + i)->front();
				(hq +i)->pop();
				parhvprocessing(G, u, d, qout);
			}
		}
		delete [] qin;
                qin = qout;
		src_bfs_level++;
        }
	

	
        checksum = parComputeChecksum(d);
        //outfile << src_bfs_level << " " << checksum << endl;
	delete [] qin;
	delete d;
}

void
bfs_parallel::processneighbors(vector<int> &neighbors, int u, vector<int> *d, int s, int e, QueueEx<int> *qo){
	for(int i = s; i < e; i++){
		int nvertices = _g->numvertices();
		if((*d)[neighbors[i]] == nvertices){
			(*d)[neighbors[i]] = (*d)[u] + 1;
			qo->push_back(neighbors[i]);
		}
	}
}
void
bfs_parallel::parhvprocessing(Graph *g, int u, vector<int> *d, QueueEx<int> *qout){
	vector<int> &neighbors = g->getNeighbors(u);
	int sz_n = neighbors.size();

	int chunksz = sz_n/_ncores;
	for(int i = 0; i < _ncores - 1; i++){ 
		cilk_spawn processneighbors(neighbors, u, d, chunksz*i, chunksz * (i+1), qout + i);
	}
	processneighbors(neighbors, u, d, chunksz * (_ncores -1), sz_n, qout + _ncores -1);
	cilk_sync;
		
}
void
bfs_parallel::setparam(int cores, int maxstealattempts, int minstealsize){
	_ncores = cores;
	_hdegree = cores * cores;
	_maxstealattempts = maxstealattempts;
	_minstealsize = minstealsize;
}
