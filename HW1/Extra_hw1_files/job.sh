		#! /bin/bash 
		#-V	 			#Inherit the submission environment
		#$ -cwd	 			# Start job in submission directory
		#$ -N a		 		# Job Name
		#$ -j y	 			# Combine stderr and stdout (If you are using this option, ignore the next line)
		#$ -e error_e		# Name of the error file in case your code is wrong
		#$ -o output_a		# Name of the output file
		#$ -pe 12way 24	 	# Requests 12 tasks/node, 24 cores total
		#$ -q normal	 	# Queue name normal
		#$ -l h_rt=01:30:00	# Run time (hh:mm:ss) - 1.5 hours
		#$ -M	 			# Address for email notification
		#$ -m be	 		# Email at Begin and End of jobexport PATH=$PATH:$HOME/cilk/bin
		#./hw1 'samples/sample-01-in.txt' 'sample-01.txt' > bfs_parallel_output 2>&1
		#./hw1 'turn-in/wikipedia-in.txt' 'wikipedia1217' > bfs_parallel_wiki1217 2>&1
		#./hw1 'turn-in/kkt_power-in.txt' 'kkt_power-out.txt' > bfs_parallel_kkt_power 2>&1
		#./bfs_parallel 'sample.txt' 'sample.out' > output 2>&1
		#./hw1 'turn-in/rmat1B-in.txt' 'rmat1B-out.txt1230' > bfs_parallel_rmat1B1230 2>&1


#./hw1 'turn-in/cage14-in.txt' 'turn-in/parbfs_cage14-out1230.txt' > 'turn-in/parbfs_cage14-outtime1230.txt' 2>&1
#./hw1 'turn-in/cage15-in.txt' 'turn-in/parbfs_cage15-out1230.txt' > 'turn-in/parbfs_cage15-outtime1230.txt' 2>&1
#./hw1 'turn-in/freescale-in.txt' 'turn-in/parbfs_freescale-out1230.txt' > 'turn-in/parbfs_freescale-outtime1230.txt' 2>&1
#./hw1 'turn-in/kkt_power-in.txt' 'turn-in/parbfs_kktpower-out1230.txt' > 'turn-in/parbfs_kktpower-outtime1230.txt' 2>&1
#./hw1 'turn-in/rmat100M-in.txt' 'turn-in/parbfs_rmat100M-out1230.txt' > 'turn-in/parbfs_rmat100M-outtime1230.txt' 2>&1
#./hw1 'turn-in/rmat1B-in.txt' 'turn-in/parbfs_rmat1B-out1230.txt' > 'turn-in/parbfs_rmat1B-outtime1230.txt' 2>&1
./hw1 'turn-in/wikipedia-in.txt' 'pbfs_wiki-out.txt' > 'pbfs_wiki-time.txt' 2>&1

#./hw2 'turn-in/cage14-in.txt' 'turn-ins/serbfs_cage14-out1230.txt' > 'turn-in/serbfs_cage14-outtime1230.txt' 2>&1
#./hw2 'turn-in/cage15-in.txt' 'turn-in/serbfs_cage15.txt' > 'turn-in/serbfs_cage15-time.txt' 2>&1
#./hw2 'turn-in/freescale-in.txt' 'turn-in/serbfs_freescale-out1230.txt' > 'turn-in/serbfs_freescale-outtime1230.txt' 2>&1
#./hw2 'turn-in/kkt_power-in.txt' 'turn-in/serbfs_kktpower-out1230.txt' > 'turn-in/serbfs_kktpower-outtime1230.txt' 2>&1
#./hw2 'turn-in/rmat100M-in.txt' 'turn-in/serbfs_rmat100M-out1230.txt' > 'turn-in/serbfs_rmat100M-outtime1230.txt' 2>&1
#./hw2 'turn-in/rmat1B-in.txt' 'serbfs_rmat1B-outOPT.txt' > 'serbfs_rmat1B-outtimeOPT.txt' 2>&1
#./hw2 'turn-in/wikipedia-in.txt' 'serbfs_wiki.txt' > 'serbfs_wiki-time.txt' 2>&1

