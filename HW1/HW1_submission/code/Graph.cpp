#include "Graph.h"
#include <iostream>

using namespace std;

Graph::Graph(int numvertices, int numedges, int numsources)
: nvertices(numvertices), nedges(numedges), nsources(numsources) {
	edges.resize(nvertices + 1);
}
Graph::~Graph(){}

void Graph::addEdge(int u, int v){
	edges[u].push_back(v);
}

void Graph::addSource(int s){
	sources.push_back(s);
}

vector<int>& Graph::getNeighbors(int u){
	return edges[u];
}

vector<int>& Graph::getSources(){
	return sources;
}

int Graph::numedges(){
	return nedges;
}

int Graph::numvertices(){
	return nvertices;
}

int Graph::numsources(){
	return nsources;
}

