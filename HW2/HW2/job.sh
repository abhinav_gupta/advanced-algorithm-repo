		#! /bin/bash 
		#-V	 			#Inherit the submission environment
		#$ -cwd	 			# Start job in submission directory
		#$ -N a		 		# Job Name
		#$ -j y	 			# Combine stderr and stdout (If you are using this option, ignore the next line)
		#$ -e error_e		# Name of the error file in case your code is wrong
		#$ -o output_a		# Name of the output file
		#$ -pe 12way 24	 	# Requests 12 tasks/node, 24 cores total
		#$ -q normal	 	# Queue name normal
		#$ -l h_rt=01:30:00	# Run time (hh:mm:ss) - 1.5 hours
		#$ -M	 			# Address for email notification
		#$ -m be	 		# Email at Begin and End of jobexport PATH=$PATH:$HOME/cilk/bin
		#./hw2 'turn-in/com-orkut-in.txt' 'orkut' > orkut.out 2>&1
		#./hw2 'turn-in/com-lj-in.txt' 'out' > lj.outcc3 2>&1
		./hw2 'turn-in/com-orkut-in.txt' 'orkutcc3.out' > orkutcc1.out 2>&1
		#./hw2 'turn-in/com-friendster-in.txt' 'out' > friend.out 2>&1
		#./hw2 'turn-in/ca-AstroPh-in.txt' 'out' > ca-AstroPh.out 2>&1
		#./hw2 'sample/com-youtube-in.txt' 'youtube' > youtube.out3 2>&1
		#./hw2 'turn-in/roadNet-PA-in.txt' 'out' > roadnet-PA.out 2>&1
		#./hw2 'turn-in/roadNet-CA-in.txt' 'out' > roadnet-CA.out 2>&1
		#./hw2 'turn-in/as-skitter-in.txt' 'out' > as-skittercc3.out 2>&1
		#./hw2 'turn-in/com-amazon-in.txt' 'out' > amazon.outcc3 2>&1
