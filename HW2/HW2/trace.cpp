#include "trace.h"
ostream debug(cout.rdbuf());
static ofstream null;

void trace_init(bool flag)
{
bool output_is_enabled = flag;
    null.open("/dev/null");
    if(!output_is_enabled) {  // put whatever your condition is here
        debug.rdbuf(null.rdbuf());
    }
}

void trace_done()
{
    null.close();
}
