#define HEAD 0
#define TAIL 1
#include <vector>
#include <cilkview.h>
#include <iostream>
#include <cmath>
#include "Graph.h"
#include "trace.h"


using namespace std;
#define DEBUG 0

#define TAIL 1
#define HEAD 0


static int* parPrefixSum(int *X, int n){
	int *S = new int[n];
	
	if(n == 1) {
		S[0] = X[0];
	} else {
		
		int *Y = new int[n/2];
		cilk_for(int i=0; i < n/2; i++){
			Y[i] = X[2*i] + X[2*i+1];
		}
		int *Z = parPrefixSum(Y, n/2);
		delete[] Y;

		cilk_for(int i=0; i < n ; i++){
			if(i == 0) {
				S[0] = X[0];
			} else if(i%2 != 0) {
				S[i] = Z[i/2];
			} else {
				S[i] = Z[(i-1)/2] + X[i];
			}
		}
		delete[] Z;
	}
	int *retS = S;
	return retS;
}


void  parRandCC(int n, int nedges, edge *E, int *L, int *M){
	
	cout<<"Par CC1 n:"<<n<<" nedges:"<<nedges<<endl;

	int start_time = cilk::get_milliseconds();
	if(nedges == 0){
		cilk_for(int i = 1; i <= n; i++){
			M[i] = L[i];
		}
		return;
	}

	int *C = new int[n+1];
	int *S = new int[nedges];
	
	cilk_for (int i=1; i <= n; i++){
		C[i] = rand()%2;
	}
	
	//tail = 1, head = 0
	cilk_for (int i=0; i < nedges; i++){
		edge cur_edge = E[i];
		if(C[cur_edge.u] == TAIL && C[cur_edge.v] == HEAD){
			L[cur_edge.u] = L[cur_edge.v];
		}
	}
	delete[] C;
	
	cilk_for (int i=0; i < nedges; i++){
		if(L[E[i].u] != L[E[i].v]){
			S[i]  = 1;
		}
		else{
			S[i] = 0;
		}
	}
	
	int* Sout = parPrefixSum(S, nedges);
	delete[] S;
	
	S = Sout;

	int newedgecnt = S[nedges - 1];
	edge *F = new edge[newedgecnt];
	cilk_for(int i=0; i<nedges; i++){
		if(L[E[i].u] != L[E[i].v]){
			F[S[i] - 1] = edge(L[E[i].u], L[E[i].v]);
		}
	}
	delete[] S;
        cout << "End :" <<  (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;
	parRandCC(n, newedgecnt,  F, L, M);
	delete[] F;

	cilk_for(int i=0; i<nedges; i++){
		edge cur_edge = E[i];
		if(cur_edge.v == L[cur_edge.u]){
			M[cur_edge.u] = M[cur_edge.v];
		}
	}
}

static void RandHook(int nvertices, int Lsize, int *V, int *L, int *N) {


	int *C = new int [Lsize];
        int *H = new int [Lsize];

        //tail = 1, head = 0
        cilk_for (int i=0; i < nvertices; i++){
                int u = V[i];
                C[u] = rand()%2;
                H[u] = false;
        }

        cilk_for (int i=0; i < nvertices; i++) {
                int u = V[i];
                int v = N[u];

                if(C[u]==TAIL && C[v]==HEAD) {
                        L[u] = v;
                        H[u] = true; 
			H[v] = true;
                }
        }

        cilk_for (int i=0; i < nvertices; i++){
                int u = V[i];

                if(H[u]==true) {
                        C[u] = HEAD;
                } else if (C[u]==TAIL) {
                        C[u] = HEAD;
                } else {
                        C[u] = TAIL;
                }
        }

        cilk_for (int i=0; i < nvertices; i++){
                int u = V[i];
                int v = N[u];
                if(C[u]== TAIL && C[v] == HEAD) {
                        L[u] = L[v];
                }
        }

	delete[] C;
	delete[] H;
}


void parRandCC2(int nvertices, int nedges, int Lsize, int* V, edge* E, int* L) {

        if(nedges == 0){
                return;
        }

        int *N = new int[Lsize];
	cilk_for(int i=0; i < nvertices; i++) {
                int u = V[i];
                N[u] = u;
	}
        
	cilk_for (int i=0; i < nedges; i++){
                edge cur_edge = E[i];
                int u = cur_edge.u; int v = cur_edge.v;
                N[u] = v;
                N[v] = u;
        }

        RandHook(nvertices, Lsize, V, L, N);
        delete[] N;

	if(DEBUG) {
		for(int i=1; i<=Lsize; i++){
			cout<<i<<" :"<<L[i]<<endl;
		}
	}

        int *S = new int[nvertices];
	cilk_for(int i=0; i < nvertices; i++) {
                int v = V[i];
                if(v == L[v]) {
                        S[i] = 1;
                } else {
			S[i] = 0;
		}
        }

        int * Sout = parPrefixSum(S, nvertices);
        delete[] S;

        S = Sout;
	Sout = NULL;

	int V_insect_sz = S[nvertices - 1];
        int *V_insect = new int[V_insect_sz];
        cilk_for(int i=0; i < nvertices; i++) {
                int v = V[i];
                if(v == L[v]) {
                        V_insect[S[i] - 1] = v;
                }
        }

	delete[] S;

	if(DEBUG) {
		for(int i=0; i<V_insect_sz; i++) {
			cout<<i<<" :"<<V_insect[i]<<"\t";
		}
		cout<<endl;
	}


       int *SE = new int[nedges];
        cilk_for(int i=0; i < nedges; i++) {
                edge cur_edge = E[i];
                int u = cur_edge.u; int v = cur_edge.v;
                if(L[u]!=L[v]) {
                        SE[i] = 1;
                } else {
                        SE[i] = 0;
                }
        }

        int* SEout = parPrefixSum(SE, nedges);
        delete[] SE;

        SE = SEout;
        SEout = NULL;

        
	int E_insect_sz = SE[nedges - 1];
        edge *E_insect = new edge[E_insect_sz];

        cilk_for(int i=0; i < nedges; i++) {
                edge cur_edge = E[i];
                int u = cur_edge.u; int v = cur_edge.v;
                if(L[u]!=L[v]) {
                        E_insect[SE[i] - 1] = edge(L[u], L[v]);
                }
        }
        delete[] SE;

	if(DEBUG) {
		for(int i=0; i<E_insect_sz; i++) {
			edge cur_edge = E_insect[i];
			int u = cur_edge.u; int v = cur_edge.v;
			cout<<i<<" :"<<"u :"<<u<<", V :"<<v<<endl;
		}
	}

	if(DEBUG) {
        	cout<<"E' Size :"<<E_insect_sz<<endl;    
	        cout<<"V' Size :"<<V_insect_sz<<endl;
	}
        parRandCC2(V_insect_sz, E_insect_sz, Lsize, V_insect, E_insect, L);
        delete[] V_insect;
        delete[] E_insect;

        cilk_for(int i=0; i<nvertices; i++){
                int v = V[i];
                L[v] = L[L[v]];
        }

        return;
}

/*
int getCorrectVertex(int v, vector<int> *L, vector<int> *V){
	if((*L)[v] != v){
		v = (*L)[v];
		return getCorrectVertex(v, L, V);
		
	} else {
		return v;
	}
	
}
*/

static void getKSamples(edge *E, int n, vector<edge> *E1, int k){
        // index for elements in stream[]

        // reservoir[] is the output array. Initialize it with
        // first k elements from stream[]
        cilk_for (int i = 0; i < k; i++) {
                (*E1)[i] = E[i];
        }

        // Use a different seed value so that we don't get
        // same result each time we run this program

        // Iterate from the (k+1)th element to nth element
        cilk_for (int i = k; i < n; i++)
        {
                // Pick a random index from 0 to i.
                int j = rand() % (i+1);

                // If the randomly  picked index is smaller than k, then replace
                // the element present at the index with new element from stream
                if (j < k)
                        (*E1)[j] = E[i];
   
        }
}

int nv=-1, ne=-1;
void parRandCC3(int nvertices, int nedges, int Lsize, int *V, edge *E, int *L, bool *phd, 
							int *N, bool *U, int d, int d_max){

	//debug << "inside parRandCC3" <<endl;
	int m = nedges;
	//debug <<"nedges=" <<nedges << ",nvertices="<< nvertices << ", d=" << d << ", d_max =" << d_max <<endl;
	cout << "\n\nne:" <<nedges << " ,nv :"<< nvertices << ", d=" << d << ", d_max =" << d_max <<endl;

	if(nv==nvertices && ne==nedges)
		d = d_max+1;
	
	nv = nvertices;
	ne = nedges;	

	int at_start_time = cilk::get_milliseconds();
	int start_time;
	if(d <= d_max){

		int m_d = ceil(m*pow(alpha1, d)); 
		//debug << "m_d: "<<m_d << "pow(alpha^d)" <<pow(alpha1,d) <<endl;
		
		int *flagV = new int[m]();
		vector<edge> *E_hat = new vector<edge>(m_d);
		/*
		int cs = 0;//current size
		
		while(cs != m_d) {
			int ind = rand()%m;
			if(flagV[ind] == 0){
				E_hat->push_back(E[ind]);
				flagV[ind] = 1;
				cs++;	
			}
			else{
				continue;
			}
		}
		*/

		start_time = cilk::get_milliseconds();
                getKSamples(E, m, E_hat, m_d);
		//debug << "size of E_hat " << E_hat->size() << endl;
		delete flagV;
                cout << "1 :" <<  (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;

		start_time = cilk::get_milliseconds();
		cilk_for(int i=0 ; i < nvertices; i++){
			int v = V[i];
			U[v] = false;
		}
                cout << "2 :" <<  (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;

		start_time = cilk::get_milliseconds();
		cilk_for(int i=0; i < m_d; i++){
			edge e = (*E_hat)[i];
			int u = e.u;
			int v = e.v;
				
			int u1 = L[u];
			int v1 = L[v];
			
			////debug << u << ", "<<v <<" || ";
			////debug << u1 << ", "<<v1<<" || ";
			////debug << phd[u1] << ", "<< phd[v1]<<endl ;
			if(u1 != v1 && phd[u1] && phd[v1]){
				N[u1] = v1;
				N[v1] = u1;
				U[v1] = true;
				U[u1] = true;
			}
		}
		delete E_hat;
                cout << "3 :" << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;


		start_time = cilk::get_milliseconds();
		cilk_for( int i = 0; i < nvertices; i++){
			int v = V[i];
			if(U[v] == false){
				phd[v] = false;
			}
		}
                cout << "4 :" <<  (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;
		

		//===================================================
		//XXX

		start_time = cilk::get_milliseconds();
		int *S = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(U[v]){
				S[i] = 1;
			} else {
				S[i] = 0;
			}
		}

		int *Sout = parPrefixSum(S, nvertices);
		delete S;

		S = Sout;
		Sout = NULL;
                cout << "5 :" << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;
		

		//===================================================

		start_time = cilk::get_milliseconds();
		int V_hat_sz = S[nvertices - 1];
		int *V_hat = new int[V_hat_sz];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(U[v]) {
				V_hat[S[i] - 1] = v;
			}
		}

		delete[] S;
                cout << "6 :" << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;


		//===================================================

		start_time = cilk::get_milliseconds();
		//debug << "Calling RandHook"<< endl;

		RandHook(V_hat_sz, Lsize, V_hat, L, N);

		cilk_for( int i = 0; i <  V_hat_sz; i++){
			int v = V_hat[i];	
			if(L[v] != v){
				phd[v] = 0;
				U[v] = 0;
			}

		}
		delete[] V_hat;
                cout << "7 :" << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;


		//===================================================
		//XXX

		start_time = cilk::get_milliseconds();
		int *SV1 = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				SV1[i] = 1;
			} else {
				SV1[i] = 0;
			}
		}

		int *SV1out = parPrefixSum(SV1, nvertices);
		delete[] SV1;
		SV1 = SV1out;
		SV1out = NULL;
                cout << "8 :" <<  (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;
		//===================================================

		start_time = cilk::get_milliseconds();
		int V1_sz = SV1[nvertices - 1];
		int *V1 = new int[V1_sz];
		

		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				V1[SV1[i] - 1] = v;
			}
		}

		delete[] SV1;
                cout << "9 :" << (((float)(cilk::get_milliseconds() - start_time))/1000) << endl;


		//===================================================
                cout << "CC3 :" << (((float)(cilk::get_milliseconds() - at_start_time))/1000) << endl;

		parRandCC3(V1_sz, nedges, Lsize, V1, E, L, phd, N, U, d+1, d_max);
		delete[] V1;
		
		cilk_for(int i=0; i < nvertices; i++){
			int v = V[i];
			//debug << v << " , " << L[v] << " , " << L[L[v]] << endl;
			L[v] = L[L[v]];
		}
	}

	if(d==0) {
		//debug << "entering cc2" << endl;

		//===================================================


		int *S = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				S[i] = 1;
			} else {
				S[i] = 0;
			}
		}

		int * Sout = parPrefixSum(S, nvertices);
		delete[] S;

		S = Sout;
		Sout = NULL;

		int V_insect_sz = S[nvertices - 1];
		int *V_insect = new int[V_insect_sz];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				V_insect[S[i] - 1] = v;
			}
		}

		//===================================================


		int *SE = new int[nedges];
		cilk_for(int i=0; i < nedges; i++) {
			edge cur_edge = E[i];
			int u = cur_edge.u; int v = cur_edge.v;
			if(L[u] != L[v]) {
				SE[i] = 1;
			} else {
				SE[i] = 0;
			}
		}

		int *SEout = parPrefixSum(SE, nedges);
		delete[] SE;

		SE = SEout;
		SEout = NULL;

		//===================================================


		int new_edge_sz = SE[nedges - 1];
		edge *E1 = new edge[new_edge_sz];

		cilk_for(int i=0; i < nedges; i++) {
			edge e = E[i];
			if(L[e.u] != L[e.v] ){
				E1[SE[i] - 1] = edge(L[e.u], L[e.v]);
			}

		}
		delete[] SE;


		parRandCC2(V_insect_sz, new_edge_sz, Lsize,V_insect, E1, L);
		delete[] V_insect;
		delete[] E1;
	        
		cilk_for(int i=0; i<nvertices; i++){
        	        int v = V[i];
                	L[v] = L[(L[v])];
        	}

	}
}
