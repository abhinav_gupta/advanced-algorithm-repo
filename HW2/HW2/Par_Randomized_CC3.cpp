#define HEAD 0
#define TAIL 1
#include <cilkview.h>
#include "Graph.h"
#include <cmath>
#include <iostream>
#include "trace.h"
#include <algorithm>
using namespace std;

//#define cilk_for for
//Replace this values as per the algo
//#define d_max 5

int getCorrectVertex(int v, vector<int> *L, vector<int> *V){
	if((*L)[v] != v){
		v = (*L)[v];
		return getCorrectVertex(v, L, V);
		
	} else {
		return v;
	}
	
}
void parRandCC3(int nvertices, int nedges, int Lsize, int *V, edge *E, int *L, bool *phd, 
							int *N, bool *U, int d, int d_max){

	debug << "inside parRandCC3" <<endl;
	int m = nedges;
	debug << "nedges=" <<nedges << ",nvertices="<< nvertices << ", d=" << d << ", d_max =" << d_max <<endl;
	
	if(d <= d_max){

		int m_d = ceil(m*pow(alpha1, d)); 
		debug << "m_d: "<<m_d << "pow(alpha^d)" <<pow(alpha1,d) <<endl;
		
		int *flagV = new int[m]();
		vector<edge> *E_hat = new vector<edge>();
		int cs = 0;//current size
		
		while(cs != m_d) {
			int ind = rand()%m;
			if(flagV[ind] == 0){
				E_hat->push_back(E[ind]);
				flagV[ind] = 1;
				cs++;	
			}
			else{
				continue;
			}
		}
				
		debug << "size of E_hat " << E_hat->size() << endl;
		delete flagV;

		cilk_for(int i=0 ; i < nvertices; i++){
			int v = V[i];
			U[v] = false;
		}

		cilk_for(int i=0; i < m_d; i++){
			edge e = (*E_hat)[i];
			int u = e.u;
			int v = e.v;
				
			int u1 = L[u];
			int v1 = L[v];
			
			debug << u << ", "<<v <<" || ";
			debug << u1 << ", "<<v1<<" || ";
			debug << phd[u1] << ", "<< phd[v1]<<endl ;
			if(u1 != v1 && phd[u1] && phd[v1]){
				N[u1] = v1;
				N[v1] = u1;
				U[v1] = true;
				U[u1] = true;
			}
		}
		delete E_hat;

		cilk_for( int i = 0; i < nvertices; i++){
			int v = V[i];
			if(U[v] == false){
				phd[v] = false;
			}
		}
		//===================================================
		//XXX

		int *S = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(U[v]){
				S[i] = 1;
			} else {
				S[i] = 0;
			}
		}

		int *Sout = parPrefixSum(S, nvertices);
		delete S;

		S = Sout;
		Sout = NULL;
		//===================================================

		int V_hat_sz = S[nvertices - 1];
		int *V_hat = new int[V_hat_sz];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(U[v]) {
				V_hat[S[i] - 1] = v;
			}
		}

		delete[] S;


		//===================================================

		debug << "Calling RandHook"<< endl;
		RandHook(V_hat_sz, Lsize, V_hat, L, N);

		cilk_for( int i = 0; i <  V_hat_sz; i++){
			int v = V_hat[i];	
			if(L[v] != v){
				phd[v] = 0;
				U[v] = 0;
			}

		}
		delete[] V_hat;


		//===================================================
		//XXX

		int *SV1 = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				SV1[i] = 1;
			} else {
				SV1[i] = 0;
			}
		}

		int *SV1out = parPrefixSum(SV1, nvertices);
		delete[] SV1;

		SV1 = SV1out;
		SV1out = NULL;
		//===================================================

		int V1_sz = SV1[nvertices - 1];
		int *V1 = new int[V1_sz];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				V1[SV1[i] - 1] = v;
			}
		}

		delete[] SV1;


		//===================================================

		parRandCC3(V1_sz, nedges, Lsize, V1, E, L, phd, N, U, d+1, d_max);
		delete[] V1;
		
		cilk_for(int i=0; i < nvertices; i++){
			int v = V[i];
			debug << v << " , " << L[v] << " , " << L[L[v]] << endl;
			L[v] = L[L[v]];
		}
	}

	if(d==0) {
		debug << "entering cc2" << endl;

		//===================================================


		int *S = new int[nvertices];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				S[i] = 1;
			} else {
				S[i] = 0;
			}
		}

		int * Sout = parPrefixSum(S, nvertices);
		delete[] S;

		S = Sout;
		Sout = NULL;

		int V_insect_sz = S[nvertices - 1];
		int *V_insect = new int[V_insect_sz];
		cilk_for(int i=0; i < nvertices; i++) {
			int v = V[i];
			if(v == L[v]) {
				V_insect[S[i] - 1] = v;
			}
		}

		//===================================================


		int *SE = new int[nedges];
		cilk_for(int i=0; i < nedges; i++) {
			edge cur_edge = E[i];
			int u = cur_edge.u; int v = cur_edge.v;
			if(L[u] != L[v]) {
				SE[i] = 1;
			} else {
				SE[i] = 0;
			}
		}

		int *SEout = parPrefixSum(SE, nedges);
		delete[] SE;

		SE = SEout;
		SEout = NULL;

		//===================================================


		int new_edge_sz = SE[nedges - 1];
		edge *E1 = new edge[new_edge_sz];

		cilk_for(int i=0; i < nedges; i++) {
			edge e = E[i];
			if(L[e.u] != L[e.v] ){
				E1[SE[i] - 1] = edge(L[e.u], L[e.v]);
			}

		}
		delete[] SE;


		parRandCC2(V_insect_sz, new_edge_sz, Lsize,V_insect, E1, L);
		delete[] V_insect;
		delete[] E1;
	        
		cilk_for(int i=0; i<nvertices; i++){
        	        int v = V[i];
                	L[v] = L[(L[v])];
        	}

	}
}
