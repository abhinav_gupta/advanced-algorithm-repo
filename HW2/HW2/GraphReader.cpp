#include "GraphReader.h"
#include <fstream>
#include <iostream>
#include <map>

using namespace std;
Graph* GraphReader::read(string filename){
	std::ifstream infile(filename.c_str());
	if(infile.is_open()) {
		int vertices = 0, edges = 0;

		infile >> vertices >> edges;
		Graph *g = new Graph(vertices, edges);
		
		int iedge = 0;
		while(iedge < edges){
			int u = 0, v = 0;
			infile >> u >> v;
			g->addEdge(u, v, iedge);
			iedge++;		
		}

	return g;
	} else {
		cout<<"Error: can't open file :"<<filename<<endl;
		exit(1);
	}
}

void GraphReader::dump(string filename, Graph *g) {
	std::ofstream outfile(filename.c_str());
	int nvertices = 0, nedges = 0;

	nvertices = g->numvertices();
	nedges = g->numedges();
	
	outfile << nvertices << " "<< nedges<<endl;

	edge *edges = g->getedge();
	for(int i=0; i < nedges; i++) {
		outfile << edges[i].u <<" "<< edges[i].v<<endl;
	}
	outfile.close();
}

void GraphReader::dumpoutput(int *M, int nvertics) {
	map<int, int> CCompnent;
	int i=1, count=0;
	for(i = 1; i<=nvertics; i++){
		(CCompnent[M[i]])++;
	}

	i = 0;
	vector <int>* cc_cnt = new vector<int>(CCompnent.size());
	
	for( map<int, int>::iterator ii=CCompnent.begin(); ii!=CCompnent.end(); ++ii)
	{
		cc_cnt->at(i) = (*ii).second;
		i++;
	}
	cout<<cc_cnt->size()<<endl;	

	sort ((*cc_cnt).begin(), (*cc_cnt).end(), greater<int>());
	for(i=0;i<CCompnent.size();i++)
		cout<<cc_cnt->at(i)<<endl;
}

